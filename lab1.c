#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace StringUtils {
	char* split(char *data, char del, int number) {
		int copyFrom = 0, curNumber = 0;
		int i = 0;
		while (data[i] != 0) {
			if (data[i] == '\0') break;
			if (data[i] == del || data[i] == '\n')
				if (number == curNumber) {
					char *res = (char*)malloc(i - copyFrom + 1);
					memcpy(res, data + copyFrom, i - copyFrom);
					res[i - copyFrom] = 0;
					return res;
				}
				else {
					++curNumber;
					copyFrom = i + 1;
				}
			i++;
		}
		char *res = (char*)malloc(i - copyFrom + 1);
		memcpy(res, data + copyFrom, i - copyFrom);
		res[i - copyFrom] = 0;
		return res;
	}

	int countSpaces(char data[]) {
		int lines = 1;
		int len = strlen(data);
		for (int i = 0; i < len; i++)
			if (data[i] == ' ' || i == len - 1)
				lines++;
		return lines - 1;
	}
}

float count(char *s) {
	float res = 0;
	char action = 'f';
	for (int i = 0; i < StringUtils::countSpaces(s); i++) {
		char* tmp = StringUtils::split(s, ' ', i);
		if (tmp[0] == '+' || tmp[0] == '-' || tmp[0] == '*')
			action = tmp[0];
		else 
			if (action == 'f')
				res = atof(tmp);
			else if (action == '+')
				res += atof(tmp);
			else if (action == '-')
				res -= atof(tmp);
			else if (action == '*')
				res *= atof(tmp);
	}
	return res;
}

int main() {
	char a[100];
	gets_s(a, 99);
	printf("%f\n\n", count(a));
	system("pause");
}