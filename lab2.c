#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define print( ss ) printf ("%s\n", ss)

bool binarySearch(int *lst, int upperBound, int x){
    int lowerBound = 0;
    while (lowerBound != upperBound){
        int comparedValue = (lowerBound + upperBound) / 2;
        if (x == lst[comparedValue])
            return true;
        else if (x < lst[comparedValue])
            upperBound = comparedValue;
        else
            lowerBound = comparedValue + 1;
    }
    return false;
}

void selectionSort(int arr[], int size) {
  for (int min=0;min<size-1;min++) {
    int least = min;
    for (int j=min+1;j<size;j++)
      if(arr[j] < arr[least])
        least = j;
    int tmp = arr[min];
    arr[min] = arr[least];
    arr[least] = tmp;
  }
}
void insertionSort(int arr[], int size){
  for (int j = 1; j < size; j++){
    int key = arr[j];
    int i = j-1;
    while(i>=0 && arr[i]>key){
      arr[i+1] = arr[i];
      --i;
    }
    arr[i+1] = key;
  }
}

int randInt(int min, int max) {
  return min + rand()%(max-min);
}

int main(int argc, char **argv) {
  if(argc == 1) {
    print("\
    Usage: first arg is sorting method.\n\
    Example: ./lab2 selection #selection sort\n\
             ./lab2 insertion #insertion sort\n");
    return 0;
  }
  char *sortMethod = argv[1];
  //print(sortMethod);
  srand(time(0));
  int cnt = 1000;
  //Cannot use scanf with "time" command
  //scanf("%d", &cnt);
  int *arr = (int*)malloc(cnt*sizeof(int));
  for (int i = 0; i < cnt; i++)
    arr[i] = randInt(0, 1337);
  if(!strcmp(sortMethod, "selection"))
    selectionSort(arr, cnt);
  else if(!strcmp(sortMethod, "insertion"))
    insertionSort(arr, cnt);
  else
    print("unk sort method, sorry\n");
  for (int i = 0; i < cnt; i++)
    printf("%d\n", arr[i]);
  
  int toFind = randInt(228, 322);
  printf("%d: ", toFind);
  print(binarySearch(arr, cnt, toFind) ? "found" : "not found");
  printf("time used: %d\n", clock());
  return 0;
}